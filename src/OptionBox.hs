module OptionBox where

import CodeWorld hiding (Point)
import Data.List (find)
import Data.Text (pack)
import DataType

xLoc = 12;

boxWidth = 7

boxHeight = 2

drawBoxElement :: String -> Picture
drawBoxElement s =
  drawText s <> thickRectangle 0.1 boxWidth boxHeight <> colored cyan (solidRectangle boxWidth boxHeight)
  where
    drawText = (scaled 0.5 0.5 . styledLettering Plain Handwriting) . pack

drawOptionBox :: [String] -> Picture
drawOptionBox xs =
  translated xLoc (fromIntegral (length xs) / 2) $
  pictures $ map (\(s, i) -> translated 0 (i * (-boxHeight)) (drawBoxElement s)) (zip xs [0 ..])

mapCoordToBoxElement :: Eq a => Point -> [a] -> Maybe a
mapCoordToBoxElement (x, y) elems
  | x <= xLoc + boxWidth / 2 && x >= xLoc - boxWidth / 2 = res
  | otherwise = Nothing
  where
    transposition = fromIntegral (length elems) / 2
    tmp = map (\(s, i) -> (s, (-boxHeight) * i + transposition)) (zip elems [0 ..])
    res = fmap fst (find (\(_, i) -> i + 1 >= y && i - 1 < y) tmp)