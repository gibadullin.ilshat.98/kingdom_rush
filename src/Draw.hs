{-# LANGUAGE OverloadedStrings #-}

module Draw where

import CodeWorld hiding (Point)
import Data.Text (pack)
import Data.Universe.Helpers

import Constants
import DataType
import Enemy (moveEnemy)
import OptionBox (drawOptionBox)

drawString :: String -> Picture
drawString = (scaled 0.5 0.5 . styledLettering Plain Handwriting) . pack

-- | draw enemy at given point of time
drawEnemy :: Time -> Enemy -> Picture
drawEnemy t (Enemy (x, y) lives (EnemyType max_lives _ _ _ _ pic) _ delay) = draw
  where
    draw 
      | t + delay < 0 = blank
      | otherwise = translated x y (red_line <> pic)
    k = fromIntegral lives / fromIntegral max_lives
    red_line = colored red (polyline [(-0.25, 0.25), (0.5 * k - 0.25, 0.25)]) <> polyline [(-0.25, 0.25), (0.25, 0.25)]

-- | draw wave at given point of time
drawWave :: Time -> Wave -> Picture
drawWave t (Wave enemies interval) = pictures (map (drawEnemy t) enemies)

drawPlaceholder :: Placeholder -> Picture
drawPlaceholder (Placeholder (x, y) tower _) = translated x y (drawTower tower)

drawRoad :: Road -> Picture
drawRoad road = colored yellow (solidPolygon segment)
  where
    segment = map fst road ++ reverse (map snd road)

drawMap:: Map -> Picture
drawMap (Map roads _ bg) = roadsPicture <> bg
  where
    roadsPicture = pictures (map drawRoad roads)

drawPlaceholders :: Map -> Picture
drawPlaceholders (Map _ placeholders _) = placeholdersPicture
  where
    placeholdersPicture = pictures (map drawPlaceholder placeholders)

drawAttackRadius :: Placeholder -> Picture
drawAttackRadius (Placeholder (x, y) towerTypeId _) 
  = translated x y (colored (translucent red) (solidCircle radius))
  where
    TowerType _ _ _ radius = getTowerType towerTypeId

drawActivity :: Activity -> Picture
drawActivity ANone = blank
drawActivity (Boom (x, y) _) = translated x y (colored red (solidCircle 1))
drawActivity (AChooseTower placeholder towerTypeId) = 
  drawAttackRadius potentialPlaceholder <>
  drawPlaceholder potentialPlaceholder <>
  drawOptionBox towersWithCostsDrawable
  where
    towersWithCostsDrawable =
      fmap (\(tower, cost) -> show tower ++ " (" ++ show cost ++ " coins)") (getTowerListWithCost placeholder)
    Placeholder coords _ _ = placeholder
    potentialPlaceholder = Placeholder coords towerTypeId 0

drawLaser :: Laser -> Picture
drawLaser (Laser src trg _) = colored red (polyline [src, trg])

drawWorld :: World -> Picture
drawWorld (Level l m waves levelMap t lasers, activity)
  | l <= 0 = drawString "You lost!"
  | null waves = drawString "You won!"
  | otherwise = drawActivity activity <> drawUI <> drawGame <> 
      drawPlaceholders levelMap <> drawLasers <> drawMap levelMap
  where
    toString = "\x2764\xfe0f" ++ show l ++ " \36" ++ show m
    drawUI = translated (-6) 4 (drawString toString)
    drawLasers = pictures (map drawLaser lasers)
    drawGame = pictures (map (drawWave t) waves)