module Enemy where

import CodeWorld hiding (Point)

import DataType
import Constants

-- | move enemy to new position based on current time
moveEnemy :: Time -> Enemy -> Enemy
moveEnemy t (Enemy _ lives (EnemyType initLives speed attackSpeed attackDamage money pic) roadMotion d) =
  Enemy (x, y) lives (EnemyType initLives speed attackSpeed attackDamage money pic) roadMotion d
  where
    (x, y) = roadMotion (speed * t)

-- | initiate wave
-- | EnemyType, Number of enemies in wave, Road, Delay between enemies, Delay between waves
initWave :: EnemyTypeId -> Int -> RoadMotion -> Time -> Time -> Wave
initWave e n road enemyDelay waveDelay =
  Wave (map (initEnemy e road . (\d -> (-d) * enemyDelay - waveDelay)) [0 .. fromIntegral n]) waveDelay

-- | initiate enemy
initEnemy :: EnemyTypeId -> RoadMotion -> Time -> Enemy
initEnemy i = Enemy (0, 0) lives (getEnemyType i)
  where
    (EnemyType lives _ _ _ _ _) = getEnemyType i

-- | get attack damage of enemy
getAttackDamage e = damage
  where
    (Enemy _ _ (EnemyType _ _ _ damage _ _) _ _) = e

-- | get reward for enemy
getReward e = reward
  where
    (Enemy _ _ (EnemyType _ _ _ _ reward _) _ _) = e

-- | check if enemy is dead
isDead :: Enemy -> Bool
isDead e = hp e <= 0