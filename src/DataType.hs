module DataType where

import CodeWorld hiding (Point)

type Lives = Int

type Money = Int

-- | Point
type Point = (Double, Double)

-- | Road, a couple of points to represent width
type Road = [(Point, Point)]

-- | Returns point, where the enemy should be at time t
type RoadMotion = Time -> Point

-- | Tower cost and attack range 
type Cost = Money

type Range = Double

-- | TowerType and Instance of a Tower
data TowerType =
  TowerType Cost AttackSpeed AttackDamage Range

data TowerTypeId
  = NoneTower
  | ArcherTower
  | MagicTower
  deriving (Eq, Show)

-- | Background picture
type Background = Picture

-- | Placeholder, where we can put a tower with lastAttackTime
data Placeholder =
  Placeholder
    { point :: Point
    , towerId :: TowerTypeId
    , lastAttackTime :: Time
    }

-- | Map : Roads Placeholders and Background
data Map =
  Map [Road] [Placeholder] Background

type Time = Double

type Speed = Time

type AttackSpeed = Time

type AttackDamage = Int

-- | EnemyType : EnemyInitLives Speed AttackSpeed AttackDamage Reward Picture
data EnemyType =
  EnemyType Lives Speed AttackSpeed AttackDamage Money Picture

-- | Enemy : EnemyPosition CurrLives TypeOfEnemy RoadMotion
data Enemy =
  Enemy
    { position :: Point
    , hp :: Lives
    , enemyType :: EnemyType
    , roadMotion :: RoadMotion
    , delay :: Time
    }

-- | Wave : enemies and spawning interval
data Wave =
  Wave [Enemy] Time

data Laser = Laser Point Point Time
-- | Level : Player lives
data Level =
  Level Lives Money [Wave] Map Time [Laser]

-- | Activity : either player chooses a tower for a placeholder or none
data Activity
  = AChooseTower Placeholder TowerTypeId
  | Boom Point Time
  | ANone

-- | World : Level and Activity
type World = (Level, Activity)