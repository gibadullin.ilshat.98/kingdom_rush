module Constants where


import CodeWorld hiding (Point)
import DataType

data EnemyTypeId
  = Warrior
  | Dog

-- | returns enemy picture by EnemyTypeId
enemyPicture :: EnemyTypeId -> Picture
enemyPicture Warrior = scaled 0.2 0.2 (colored gray (solidCircle 1))
enemyPicture Dog = scaled 0.2 0.2 (colored brown (solidRectangle 2 1))

-- | get EnemyType : EnemyInitLives Speed AttackSpeed AttackDamage Reward Picture
getEnemyType :: EnemyTypeId -> EnemyType
getEnemyType Warrior = EnemyType 6 2 1 2 20 (enemyPicture Warrior)
getEnemyType Dog = EnemyType 3 4 1 1 10 (enemyPicture Dog)


-- | radius of placeholder
placeholderRadius = 0.5

-- | returns tower picture by TowerTypeId
drawTower :: TowerTypeId -> Picture
drawTower NoneTower = colored white (solidCircle placeholderRadius)
drawTower MagicTower = colored blue (solidCircle placeholderRadius)
drawTower ArcherTower = colored brown (solidCircle placeholderRadius)

-- | get TowerType : Cost AttackSpeed AttackDamage Range
getTowerType :: TowerTypeId -> TowerType
getTowerType NoneTower = TowerType 0 0 0 0
getTowerType MagicTower = TowerType 50 1 1 2
getTowerType ArcherTower = TowerType 50 1 1 2


towersList = [NoneTower, ArcherTower, MagicTower]

-- | costs of buying another Tower
getTowerListWithCost :: Placeholder -> [(TowerTypeId, Cost)]
getTowerListWithCost (Placeholder _ NoneTower _) = zip towersList [0, -70, -80]
getTowerListWithCost (Placeholder _ ArcherTower _) = zip towersList [50, 0, -30]
getTowerListWithCost (Placeholder _ MagicTower _) = zip towersList [50, -20, 0]

