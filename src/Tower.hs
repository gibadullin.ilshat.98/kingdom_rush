module Tower where

import CodeWorld hiding (Point)

import Constants
import DataType
import Util

damageEnemy :: Lives -> Enemy -> Enemy
damageEnemy damage (Enemy p hp et rm d) = Enemy p (hp - damage) et rm d

attackEnemyByPlaceholder :: Time -> Placeholder -> Enemy -> (Placeholder, Enemy, Maybe Laser)
attackEnemyByPlaceholder time placeholder enemy =
  (newPlaceholder, newEnemy, laser)
  where
    enemyPoint = position enemy
    (Placeholder towerPoint towerTypeId lastAttackTime) = placeholder
    newPlaceholder = Placeholder towerPoint towerTypeId newLastAttackTime
    newLastAttackTime
      | dist towerPoint enemyPoint < range && time - lastAttackTime >= 1 / attackSpeed = time
      | otherwise = lastAttackTime
      
    laser
      | dist towerPoint enemyPoint < range && time - lastAttackTime >= 1 / attackSpeed 
        = Just (Laser towerPoint enemyPoint (time + 3))
      | otherwise = Nothing
      
    (TowerType _ attackSpeed damage range) = getTowerType towerTypeId
    newEnemy
      | dist towerPoint enemyPoint < range && time - lastAttackTime >= 1 / attackSpeed = damageEnemy damage enemy
      | otherwise = enemy

attackWaveByPlaceholder :: Time -> Placeholder -> [Wave] -> (Placeholder, [Wave], Maybe Laser)
attackWaveByPlaceholder t p [] = (p, [], Nothing)
attackWaveByPlaceholder t p waves = (newPlaceholder, newWaves, laser)
  where
    closest e1 e2
      | dist (position e1) (point p) < dist (position e2) (point p) = e1
      | otherwise = e2
    enemies = concatMap (\(Wave es _) -> es) waves
    closestEnemy = foldr1 closest enemies
    (newPlaceholder, newClosestEnemy, laser) = attackEnemyByPlaceholder t p closestEnemy

    setClosest e
      | position e == position closestEnemy = newClosestEnemy
      | otherwise = e
    newWaves = map (\(Wave es d) -> Wave (map setClosest es) d) waves

attackByPlaceholders :: Time -> [Placeholder] -> [Placeholder] -> [Laser] -> [Wave] -> ([Placeholder], [Wave], [Laser])
attackByPlaceholders t [] phs ls ws = (phs, ws, ls)
attackByPlaceholders t (p:phs) phsAttacked lasers waves =
  attackByPlaceholders t phs (newPlaceholder:phsAttacked) (newLasers newLaser) newWaves
  where
    newLasers Nothing = lasers
    newLasers (Just laser) = laser : lasers
    (newPlaceholder, newWaves, newLaser) = attackWaveByPlaceholder t p waves

attackEnemies :: Time -> [Placeholder] -> [Wave] -> ([Placeholder], [Wave], [Laser])
attackEnemies t phs = attackByPlaceholders t phs [] []