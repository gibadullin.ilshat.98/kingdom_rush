{-# OPTIONS_GHC -Wall #-}

module KingdomRush where

import CodeWorld hiding (Point)
import Data.List
import Data.Maybe
import Prelude hiding (map)

import Constants
import DataType
import Draw (drawWorld)
import Enemy
import Map (getFinish, getMap, roadMotion1, roadMotion2)
import OptionBox (mapCoordToBoxElement)
import Tower
import Util

initWorld :: World
initWorld = (Level 10 150 waves getMap 0.0 [], ANone)
  where
    waves =
      [ initWave Warrior 7 roadMotion1 5 0
      , initWave Dog 4 roadMotion2 2 0
      , initWave Dog 4 roadMotion2 2 30
      , initWave Dog 4 roadMotion2 2 60
      ]

-- | check if enemy reached finish
hasReached :: Enemy -> Bool
hasReached (Enemy p _ _ _ _) = dist p getFinish < 1

filterWaves :: (Enemy -> Bool) -> [Wave] -> [Wave]
filterWaves f w = filter (\(Wave es _) -> not (null es)) (fmap (\(Wave es d) -> Wave (filter f es) d) w)

moveWave ::Time -> Wave -> Wave
moveWave t (Wave enemies d) = movedWave
  where
    times = map delay enemies
    movingIntervals = fmap (moveEnemy . (+ t)) times
    movedWave = Wave [movingInterval enemy | (movingInterval, enemy) <- zip movingIntervals enemies] d
  
updateWorld :: Double -> World -> World
updateWorld t (Level lives money waves levelMap time lasers, activity) =
  (Level newLives newMoney finalWaves newLevelMap newTime finalLasers, getActivity activity)
  where
    newTime = time + t
    movedWaves = map (moveWave newTime) waves

    Map roads phs bg = levelMap
    (newPhs, damagedWaves, newLasers) = attackEnemies newTime phs movedWaves

    newLevelMap = Map roads newPhs bg
    finalLasers = newLasers ++ filter (\(Laser _ _ tt) -> tt <= newTime) lasers
    finalWaves = filterWaves (\e -> not (hasReached e || isDead e)) damagedWaves

    reached = concatMap (\(Wave es _) -> filter hasReached es) damagedWaves
    dead = concatMap (\(Wave es _) -> filter isDead es) damagedWaves
    newLives = lives - sum (fmap getAttackDamage reached)
    newMoney = money + sum (fmap getReward dead)
    
    getActivity (Boom coord tt)
      | tt - t > 0 = Boom coord (tt - t)
      | otherwise = ANone
    getActivity a = a

isInPlaceholder :: Point -> Placeholder -> Bool
isInPlaceholder coord (Placeholder phCoord _ _) = dist coord phCoord < placeholderRadius

tryPressPlaceholder :: Point -> World -> World
tryPressPlaceholder coord (Level lives money waves mp time lasers, AChooseTower placeholder _) =
  (Level lives newMoney waves newMap time lasers, ANone)
  where
    towerWithCost = mapCoordToBoxElement coord (getTowerListWithCost placeholder)
    tower = fmap fst towerWithCost
    cost = maybe 0 snd towerWithCost
    newMoney
      | isJust tower && money + cost >= 0 = money + cost
      | otherwise = money
    Map roads placeholders bg = mp
    newPlaceholders = fmap mapper placeholders
    mapper currPlaceholder
      | point currPlaceholder == point placeholder && isJust tower && money + cost >= 0 =
        Placeholder (point placeholder) (fromJust tower) 0
      | otherwise = currPlaceholder
    newMap = Map roads newPlaceholders bg
tryPressPlaceholder coord (Level lives money waves mp time lasers, _) =
  (Level lives money finalWaves (Map roads placeholders bg) time lasers, getActivity)
  where
    Map roads placeholders bg = mp
    
    damageIfInRange e
      | dist (position e) coord < 1 = damageEnemy 2 e
      | otherwise = e
    damageWave (Wave enemies d) = Wave (map damageIfInRange enemies) d
    finalWaves = map damageWave waves
    
    getActivity
      | isJust placeholder = AChooseTower (fromJust placeholder) NoneTower
      | otherwise = Boom coord 0.1
      where
        placeholder = find (isInPlaceholder coord) placeholders
    
tryShowTower :: Point -> World -> World
tryShowTower coord (Level lives money waves mp time lasers, AChooseTower placeholder _) 
  = (Level lives money waves mp time lasers, AChooseTower placeholder newTowerTypeId)
  where
    towerWithCost = mapCoordToBoxElement coord (getTowerListWithCost placeholder)
    newTowerTypeId = maybe NoneTower fst towerWithCost
tryShowTower _ world = world

handleWorld :: Event -> World -> World
handleWorld (PointerPress mouse) = tryPressPlaceholder mouse
handleWorld (PointerMovement mouse) = tryShowTower mouse
handleWorld _ = id

run :: IO ()
run = interactionOf initWorld updateWorld handleWorld drawWorld