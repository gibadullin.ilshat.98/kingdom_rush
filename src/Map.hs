module Map where

import CodeWorld hiding (Point)
import DataType

-- | get background of game
getBackground :: Background
getBackground = thickRectangle 0.1 16 10 <> colored green (solidRectangle 16 10)

-- | first road motion function
roadMotion1 :: Time -> Point
roadMotion1 t = (x, y)
  where
    time =
      if 7.0 - t / 10 > 1.2
        then t / 10
        else 3.87 + t / 30
    x = 7.0 - time
    y = -4.829906 + 11.25954 * x - 5.589734 * x ^ 2 + 1.32586 * x ^ 3 - 0.1481255 * x ^ 4 + 0.006253464 * x ^ 5

-- | second road motion function
roadMotion2 :: Time -> Point
roadMotion2 t = (x, y)
 where
   (x, y) = if t/8 - 8.0 <  0.5 then (t/8 - 8.0, -0.5) else (0.5, 8.0 - t/8)

-- | first road pic
getRoad1 :: Road
getRoad1 =
  [ ((8, 5), (8, 4))
  , ((7, 4.95), (7, 3.95))
  , ((6, 4.9), (6, 3.9))
  , ((5, 4.85), (5, 3.85))
  , ((4, 4.75), (4, 3.75))
  , ((3, 4.6), (3, 3.5))
  , ((2, 4.3), (2, 2.95))
  , ((1, 3.45), (1, 0.1))
  , ((0, 0), (1, 0))
  , ((0, -5), (1, -5))
  ]

-- | second road pic
getRoad2 :: Road
getRoad2 = [((-8, 0), (-8, -1)), ((0, 0), (0, -1)), ((0.7, -0.3), (0, 0)), ((1, -1), (0, -1)), ((1, -5), (0, -5))]

-- | finish point
getFinish :: Point
getFinish = (0.5, -5.5)

-- | placeholder positions
getPlaceholders :: [Placeholder]
getPlaceholders =
  [ Placeholder (5.7, 3.2) NoneTower 0
  , Placeholder (2.7, 2.6) NoneTower 0
  , Placeholder (-0.5, 0.5) NoneTower 0
  , Placeholder (-3.5, 0.5) NoneTower 0
  , Placeholder (-6.7, 0.5) NoneTower 0
  , Placeholder (-5, -1.5) NoneTower 0
  , Placeholder (-2, -1.5) NoneTower 0
  , Placeholder (1.7, -1.5) NoneTower 0
  ]

getMap :: Map
getMap = Map [getRoad1, getRoad2] getPlaceholders getBackground